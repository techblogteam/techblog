import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../auth.service';
import { UserLogin } from '../model/user-login.interface';
import { UserLoginDto } from '../model/user-login.dto';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss'],
    providers: [AuthenticationService]
})

export class LoginComponent implements OnInit {

    userLogin: UserLogin = new UserLoginDto();
    constructor( private authService: AuthenticationService ) { }

    ngOnInit() { }
    clickLogin() {
        console.log('asd', this.userLogin);
        this.authService.login(this.userLogin, (success: any) => {
            console.log('success', success);
        }, (error: any) => {
            console.log('fail', error);
        });
    }
}
