import { NgModule } from '@angular/core';

import { RegistrationComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [ FormsModule ],
    exports: [],
    declarations: [
        RegistrationComponent,
        LoginComponent
    ],
    providers: [],
})
export class AuthenticationModule { }
