import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../shared/service/core.service';


@Injectable()
export class AuthenticationService extends BaseService{

    constructor( httpClient: HttpClient ) {
        super(httpClient);
    }
    login(userLogin, successCallback, errorCallback) {
        this.postquery('auth/login', userLogin, (success: any) => {
            successCallback(success);
        }, (error: any) => {
            errorCallback(error);
        });
    }
}
