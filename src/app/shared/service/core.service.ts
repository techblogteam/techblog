import { Injectable } from '@angular/core';
import { HttpHeaders  } from '@angular/common/http';
@Injectable()
export class BaseService{
    // tslint:disable-next-line: no-inferrable-types
    url: string = 'http://localhost:9090/';
    constructor( private http) {
    }


    get requestOptions()  {
        const headers  = new HttpHeaders({ 'Content-Type': 'application/json' });
        const options  = {headers};
        return options;
    }
    buildQuery( params: any ) {
        return this.http_build_query( params );

    }
    urlencode(str: any) {
        str = (str + '');
        return encodeURIComponent(str)
            .replace(/!/g, '%21')
            .replace(/'/g, '%27')
            .replace(/\(/g, '%28')
            .replace(/\)/g, '%29')
            .replace(/\*/g, '%2A')
            .replace(/%20/g, '+');
    }
    http_build_query(formdata: any, numericPrefix= '', argSeparator= '') {
        const urlencode = this.urlencode;
        let value: any;
        let key: any;
        const tmp: any = [];
        // tslint:disable-next-line: variable-name
        // tslint:disable-next-line: only-arrow-functions
        // tslint:disable-next-line: variable-name
        const _httpBuildQueryHelper = function(key: any, val: any, argSeparator: any) {
            let k: any;
            // tslint:disable-next-line: no-shadowed-variable
            const tmp: any = [];
            if (val === true) {
                 val = '1';
            } else if (val === false) {
                val = '0';
            }
            if (val !== null) {
                if (typeof val === 'object') {
                    for (k in val) {
                        if (val[k] !== null) { tmp.push(_httpBuildQueryHelper(key + '[' + k + ']', val[k], argSeparator)); }
                    }
                    return tmp.join(argSeparator)
                } else if (typeof val !== 'function') {
                    return urlencode(key) + '=' + urlencode(val);
                } else {
                    throw new Error('There was an error processing for http_build_query().');
                }

            } else { return ''; }
        };

        if (!argSeparator) { argSeparator = '&'; }
        // tslint:disable-next-line: forin
        for (key in formdata) {
            value = formdata[key];
            if (numericPrefix && !isNaN(key)) { key = String(numericPrefix) + key; }
            const query = _httpBuildQueryHelper(key, value, argSeparator);
            if (query !== '') { tmp.push(query); }

        }

        return tmp.join(argSeparator);
    }
    getUrl( qs: string = '' ) {
        return this.url + qs;
    }
    postquery( api, data: any, successCallback: any, errorCallback: any ) {
        const body = data;
        console.log('---_', data);
        this.http.post( this.getUrl(api), body, this.requestOptions )
            // tslint:disable-next-line: no-shadowed-variable
            .subscribe( (data: { [x: string]: any; _body: string; }) => {
                try {
                    console.log('data', data);
                    if (data.status === 200) {
                        successCallback( data );
                    } else {
                        errorCallback( data );
                    }
                } catch ( e ) {
                    errorCallback( data );
                }
            });
    }
    gettquery( api: string, data, successCallback: any, errorCallback: any ) {
        const body = this.buildQuery(data);
        this.http.get( this.getUrl(api), body, this.requestOptions )
            // tslint:disable-next-line: no-shadowed-variable
            .subscribe( (data: { [x: string]: any; _body: string; }) => {
                try {
                    const re = JSON.parse( data._body );
                    if ( re[0].code ) { return errorCallback( re[0] ); }
                    successCallback( re );
                } catch ( e ) {
                    errorCallback( data._body );
                }
            });
    }
}
